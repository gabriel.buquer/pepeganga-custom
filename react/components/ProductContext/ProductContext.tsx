import React from 'react'
import { useProduct } from "vtex.product-context"

const ProductContext = () => {
    const ProductContext = useProduct();
    console.log(ProductContext)
    return (
        <div>
            {ProductContext?.product?.brand}
        </div>
    )
}

export default ProductContext
