import React from 'react'
// import { canUseDOM } from 'vtex.render-runtime'
import { useProduct } from 'vtex.product-context'

import { useQuery } from 'react-apollo'

// import getSession from "../../graphql/getSession.graphql"
import product from "../../graphql/product.graphql"

const ExampleBlock = () => {
  const ProductContext = useProduct()
  // if(!canUseDOM) return <></>

  // const { data, loading, error } = useQuery(getSession);

  const { data } = useQuery(product, {
      variables: {
        productId: ProductContext?.product?.productId
      },
      ssr: false
  });

  // const handleClick = () => getProduct({
  //   variables: {
  //     productId: ProductContext?.product?.productId
  //   }
  // });

  // if(!data) return <></>

  console.log(data);

  return <div>
    {!data?.product?.isActive ? data?.product?.releaseDate : data?.product?.salesChannel[0].countryCode}
    {
      // data.categories.map(category: any, idx: number) => {
      //   return <>
      //       <span key={idx}>{category.name}</span>
      //     </>
      // })
    }
  </div>
}

export default ExampleBlock
