import React from 'react'
// import { canUseDOM } from 'vtex.render-runtime'

import { useLazyQuery } from 'react-apollo'

// import getSession from "../../graphql/getSession.graphql"
import products from "../../graphql/products.graphql"

const ExampleBlock = () => {

  // if(!canUseDOM) return <></>

  // const { data, loading, error } = useQuery(getSession);

  const [getProducts, { data }] = useLazyQuery(products, {
      ssr: false
  });

  const handleClick = () => getProducts({
    variables: {
      collectionId: "140",
      page: 1,
      pageSize: 6,
      term: "Camisa"
    }
  });

  // if(!data) return <></>

  console.log(data);

  return <div>
    <button onClick={handleClick}> Click me </button>
    {
      // data.categories.map(category: any, idx: number) => {
      //   return <>
      //       <span key={idx}>{category.name}</span>
      //     </>
      // })
    }
  </div>
}

export default ExampleBlock
