import React from 'react'
import { useCssHandles } from 'vtex.css-handles'

import classNames from 'classnames'
import styles from "./SiteEditorIntegration.css"
interface SiteEditorIntegrationProps {
  text: string
  isH1: boolean
  items: any
}

const CSS_HANDLES = ['exampleBlock__paragraph']

const SiteEditorIntegration: StorefrontFunctionComponent = ({
  text,
  isH1,
  items
}: SiteEditorIntegrationProps) => {
  console.log(items)
  const cssHandles = useCssHandles(CSS_HANDLES);
  return (
    <div className={classNames(styles.exampleBlock, 'b--dark-pink')}>
      {isH1 ? (
        <h1 className={styles.exampleBlock__title}>{text}</h1>
      ): (
        <span className={classNames(styles.exampleBlock__span, 'f1')}>{text}</span>
      )}
      <p className={cssHandles.exampleBlock__paragraph}> Lorem ipsum </p>

      
      {items?.map((item: any) => {
        return (
          <div>
            <span>{item.text}</span>
            <img src={item.image} />
          </div>
        )
      })}
    </div>
  )
}

SiteEditorIntegration.schema = {
  title: 'Site Editor Integration'
}

export { SiteEditorIntegration }
